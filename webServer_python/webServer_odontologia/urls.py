from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
from services.testPetition import tespetition1
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^testpetition/', tespetition1),
    (r'^login/$', 'django.contrib.auth.views.login'),
    # Uncomment the admin  /doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

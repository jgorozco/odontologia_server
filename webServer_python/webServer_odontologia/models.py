from django.db import models
from django.contrib.auth.models import User



class ClinicAdmin(models.Model):
    django_user=models.ForeignKey(User)#deberia ser el objeto de user
    name = models.CharField(max_length=50)
    surName = models.CharField(max_length=50)
    

class Clinic(models.Model):
    clinicOwner=models.ForeignKey(ClinicAdmin)#deberia ser el objeto de user
    groupName = models.CharField(max_length=50)
  
class Doctor(models.Model):
    django_user=models.ForeignKey(User)#deberia ser el objeto de user
    myClinic=models.ForeignKey(User)
    name=models.CharField(max_length=50)
    
class Paciente(models.Model):
    name = models.CharField(max_length=50)
    surName = models.CharField(max_length=50)
    myDoctor=models.ForeignKey(Doctor)
    medicalHistory=models.TextField()
    
##########################################
#########Clases con datos estaticos#######
##########################################
class Sector(models.Model):
    sectorName=models.CharField(max_length=50)
    
class Especialidad(models.Model):
    sectorOwner=models.ForeignKey(Sector)
    specialityName=models.CharField(max_length=50)

class Tratamiento(models.Model):
    sectorOwner=models.ForeignKey(Especialidad)
    tratName=models.CharField(max_length=50)
    tratDescriptor=models.TextField()
    tratContraindic=models.CharField(max_length=250)
    precio=models.FloatField()
    
class SectoresDoctor(models.Model):
    doctorSector=models.ForeignKey(Doctor)
    sectorDoctor=models.ForeignKey(Sector)
    dateCreate=models.CharField(max_length=50)

class EspecialidadesDoctor(models.Model):
    doctorEspecialidad=models.ForeignKey(Doctor)
    especialidadDoctor=models.ForeignKey(Especialidad)
    dateCreate=models.CharField(max_length=50)

class TratamientosDoctor(models.Model):
    doctorTratamiento=models.ForeignKey(Doctor)
    tratamientoDoctor=models.ForeignKey(Especialidad)
    dateCreate=models.CharField(max_length=50)
       
class Producto(models.Model):
    name=models.CharField(max_length=50)
    vendor=models.CharField(max_length=50)
    precio=models.FloatField()
    stock=models.IntegerField()
     
class ProductosTratamientos(models.Model):
    productoTratamiento=models.ForeignKey(Producto)
    tratamientoProducto=models.ForeignKey(Tratamiento)
    cantidad=models.IntegerField()
    
#############################
############################
class Cita(models.Model):
    myDoctor=models.ForeignKey(Doctor)
    paciente=models.ForeignKey(Paciente)
    initDateHour=models.ForeignKey()

class Factura(models.Model):
    paciente=models.ForeignKey(Paciente)
    fromCita=models.ForeignKey(Cita)
    ammount=models.FloatField()
    status=models.CharField(max_length=50)
    
class Gasto(models.Model):
    product=models.ForeignKey(Producto)
    ammount=models.FloatField()
    status=models.CharField(max_length=50)    

'''
Created on 22/03/2012

@author: jgorozco
'''
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User,Group
import logging
from webServer_odontologia.models import *


@login_required(login_url='/login')
def tespetition1(request):
    if request.user:
        logging.info('recibido user:'+str(request))
    if request.user.is_authenticated():
        returnedData=""
        logging.info('userAuth::'+ request.user.email)
        man=managePetitions()
#        man.funcionCreacionUsuario(request.user)
#        man.funcionCreacionTags()
        returnedData=man.functionCreacionGroup(request.user)
        return HttpResponse(str(returnedData))
    else:
        return HttpResponseRedirect('/login/?next=%s' % request.path)
    #logging.info('recibida peticion:'+str(request))
    return HttpResponse("Hello world2")


class managePetitions():
    def funcionCreacionUsuario(self,localuser):
        logging.info('localuser::'+localuser.email)
        userdata,created=UserData.objects.get_or_create(django_user=localuser)
        logging.info('localuser::'+str(created))
        if created:
            logging.info('NO existe usuario en db')
            userdata.name='pepito'
            userdata.surName='piscinas'
            userdata.django_user=localuser
            userdata.save()   
        else:
            logging.info('existe usuario en db')

    def funcionCreacionTags(self):
        tag1,created=Tag.objects.get_or_create(tagName="colegio fulanito")
        tag1.tagName="colegio fulanito"
        tag1.isBaseTag=True
        tag1.parentTag_id="-1"
        tag1.save()
        tag2,created=Tag.objects.get_or_create(tagName="grupo ciencias sociales")
        tag2.tagName="grupo ciencias sociales"
        tag2.isBaseTag=False
        tag2.parentTag_id=str(tag1.id)
        tag2.save()
        tag3,created=Tag.objects.get_or_create(tagName="Clase ciencias sociales 1")
        tag3.tagName="Clase ciencias sociales 1"
        tag3.isBaseTag=False
        tag3.parentTag_id=str(tag2.id)       
        tag3.save()

    def functionCreacionGroup(self,localuser):
        stringger="user :"+localuser.email
        for groupp in localuser.groups.all():
            if groupp.id==1:
                groupdata,created=GroupData.objects.get_or_create(django_group=groupp)
                if created:
                    logging.info('NO existe grupo en db')
                    groupdata.django_group=groupp
                    groupdata.groupName="administrador absoluto"
                    groupdata.save()
            if groupp.id==2:
                groupdata,created=GroupData.objects.get_or_create(django_group=groupp)
                if created:
                    logging.info('NO existe grupo en db')
                    groupdata.django_group=groupp
                    groupdata.groupName="administrador Colegio"
                    groupdata.save()
            stringger=stringger+" grupo:"+str(groupp.id)
        #TODO crear un grupo de usuario. aniadirle un grupo normal
        logging.info(stringger)
        return stringger


